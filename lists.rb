# why did i write this lone file in a repo?
class Node
	attr_accessor :datum

	def initialize(datum=nil, next_node=nil)
		@datum = datum
		@next = next_node
	end

	def next
		@next
	end

	def next=(node)
		@next = node
	end

	# this is really neat-o
	def << node
		if @next.nil?
			@next = node
		else
			@next << node
		end
	end

	def sort
		return if @next.nil?

		if @datum > @next.datum
			@datum, @next.datum = @next.datum, @datum
		end
		@next.sort
	end

	def to_s
		if @next
			print "#{@datum} -> "
			@next.to_s
		else
			puts "#{@datum}"
		end
	end
end
