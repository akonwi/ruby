class Node {
  constructor(datum, next) {
    this._datum = datum;
    this._next = next
  }

  get next() {
    return this._next
  }

  get datum() {
    return this._datum;
  }
}
